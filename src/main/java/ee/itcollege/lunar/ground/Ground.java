package ee.itcollege.lunar.ground;

import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class Ground extends Polygon {

	public Ground(double width, double height) {
		
		createMountains(width, height);
		
		setFill(Color.GREY);
		
	}
	
	private void createMountains(double width, double height) {
		ObservableList<Double> points = getPoints();
		points.addAll(width, height);
		points.addAll(0., height);
		
		double y = 300;
		
		for (double i = 0; i <= width + 3; i+= 3) {
			double x = i;
			double trend = height - Math.cos((i / height) * Math.PI * 4) * 100 - 100;
			y = y + Math.random() * 12 - 6 + (trend - y > 0 ? 3 : -3);
			y = Math.min(height - 20, y);
			y = Math.max(100, y);
			points.addAll(x, y);
		}
	}
	
}

