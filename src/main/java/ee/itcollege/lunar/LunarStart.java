package ee.itcollege.lunar;

import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ee.itcollege.lunar.aspect.ForceAspect;
import ee.itcollege.lunar.ground.Ground;
import ee.itcollege.lunar.lib.Vector;
import ee.itcollege.lunar.object.SpaceShip;

public class LunarStart extends Application {
	
	private static final Logger logger = LogManager.getLogger(ForceAspect.class);

	SpaceShip spaceShip = new SpaceShip();
	Ground ground = new Ground(500, 500);
	
	Vector gravity = new Vector(0, .1);
	
	
	private static void exit(WindowEvent e) {
		System.exit(0);
	}
	
	
	@Override
	public void start(Stage window) throws Exception {
		
		Pane layout = new Pane();

		ObservableList<Node> children = layout.getChildren();
		
		children.add(ground);
		
		spaceShip.setLayoutX(250);
		spaceShip.setLayoutY(150);
		children.add(spaceShip);
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				spaceShip.applyForce(gravity);
				spaceShip.move();
			}
		}, 70, 70);
		
		window.addEventFilter(KeyEvent.KEY_PRESSED, (event) -> {
			switch (event.getCode()) {
			case LEFT:
				spaceShip.rotate(-10);
				break;
			case RIGHT:
				spaceShip.rotate(10);
				break;
			case UP:
				spaceShip.accelerate();
				break;
			case DOWN:
			case SPACE:
//				spaceShip.layBomb(); // TODO
				break;
			case CONTROL:
//				spaceShip.fire(); // TODO
				break;
			case ESCAPE:
				exit(null);
				
			default:
				break;
			}
			
			if (!Shape.intersect(spaceShip.getShape(), ground).getBoundsInLocal().isEmpty()) {
				logger.debug("Collision with ground!");
			}
		});
		
		Scene scene = new Scene(layout, 500, 500);
		window.show();
		window.setTitle("Lunar landing");
		window.setResizable(false);
		window.setScene(scene);
		window.setOnCloseRequest(LunarStart::exit);
	}
	
	public static void main(String[] args) {
		
		launch(args);
	}

}