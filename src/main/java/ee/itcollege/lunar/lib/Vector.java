package ee.itcollege.lunar.lib;

public class Vector {

	private double x = 0;
	private double y = 0;
	
	public Vector() {}
	
	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void add(Vector other) {
		this.setX(this.getX() + other.getX());
		this.setY(this.getY() + other.getY());
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
