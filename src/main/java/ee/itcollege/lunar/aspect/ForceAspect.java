package ee.itcollege.lunar.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class ForceAspect {

	private static final Logger logger = LogManager.getLogger(ForceAspect.class);
	
	@Before("execution(* *(ee.itcollege.lunar.lib.Vector))")
	public void testing(JoinPoint jp) {
		logger.info("force used: " + jp.getSignature());
	}
	
}
