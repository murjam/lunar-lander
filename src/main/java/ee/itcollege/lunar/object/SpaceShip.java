package ee.itcollege.lunar.object;

import javafx.scene.Group;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import ee.itcollege.lunar.lib.Vector;

public class SpaceShip extends Group {
	
	private Vector speed = new Vector();
	private Polygon polygon = new Polygon();

	public SpaceShip() {

		polygon.getPoints().addAll(
				0., 20.,
				10., 0.,
				20., 20.,
				10., 15.
		);
		
		getChildren().add(polygon);
		
	}
	
	public Shape getShape() {
		return polygon;
	}
	
	public void applyForce(Vector force) {
		speed.add(force);
	}
	
	public void rotate(double degrees) {
		polygon.setRotate(polygon.getRotate() + degrees);
	}
	
	public void move() {
		setLayoutX(getLayoutX() + speed.getX());
		setLayoutY(getLayoutY() + speed.getY());
	}
	
	public void accelerate() {
		
		double degrees = polygon.getRotate();
		double radians = degrees / 180 * Math.PI;
		
		applyForce(new Vector(Math.sin(radians), -Math.cos(radians)));
	}
	
}
